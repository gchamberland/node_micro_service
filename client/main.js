import React from 'react'
import { render } from 'react-dom'

function getHTTPObject() {
	if (typeof XMLHttpRequest != 'undefined') {
		return new XMLHttpRequest();
	} try {
		return new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			return new ActiveXObject("Microsoft.XMLHTTP");
		} catch (e) {}
	}
	return false;
}

var ListEpisode = React.createClass({
		
		getInitialState: function(){
			return { episodes: [] };
		},
		
		loadEpisodes: function(){
			var http = getHTTPObject();

			if (!http)
				return;
				
			http.open("GET", "http://127.0.0.1:9312/", true);
			http.onreadystatechange = function (){
				if (http.readyState != 4)
					return;
				
				if (http.status == 204)
					return;
				
				try{
					this.setState({ episodes: JSON.parse(http.responseText) });
				}catch (e) {
					console.error("Impossible de parser la réponse du serveur : " +e);
				}
			}.bind(this);
			http.send(null);
		},
		
		componentDidMount: function () {
			this.loadEpisodes();
			if (this.props.pollInterval)
				setInterval(this.loadEpisodes, this.props.pollInterval);
		},
		
		render: function () {
			return (
				<table className={"col3"}>
					<thead>
						<tr>
							<th>Titre :</th>
							<th>Saison :</th>
							<th>Episode :</th>
						</tr>
					</thead>
					<tbody>
						{this.state.episodes.map(function (episode)
							{
								return (
									<tr key={episode.id}>
										<td>{episode.title}</td>
										<td>{episode.season}</td>
										<td>{episode.episode}</td>
									</tr>
								);
							})
						}
					</tbody>
				</table>
			);
	}
});

var EpisodeForm = React.createClass({
	
	getInitialState: function(){
		return { 
			title: '',
			episode: '',
			season: '',
			submittable: false
		};
	},
	
	isFormComplet: function(){
		var title = this.state.title.trim();
		var season = this.state.season.trim();
		var episode = this.state.episode.trim();
		
		return (0 != title.length && 0 != season.length && 0 != episode.length);
	},
	
	checkFormIsComplet: function (){
		this.setState({submittable: this.isFormComplet()});
	},
	
	handleTitleChange: function(e){
		this.setState({title: e.target.value});
		this.checkFormIsComplet();
	},
	
	handleSeasonChange: function(e){
		this.setState({season: e.target.value});
		this.checkFormIsComplet();
	},
	
	handleEpisodeChange: function(e){
		this.setState({episode: e.target.value});
		this.checkFormIsComplet();
	},
	
	handleSubmit: function(){
		if (this.isFormComplet()) {
			this.props.onEpisodeSubmit(this.state.title.trim(), this.state.season.trim(), this.state.episode.trim());
			this.setState({title: '', season: '', episode: ''});
		}
	},
	
	render: function (){
		return (
			<form className={"col1"}>
				<div>
					<input name="title" id="title" type="text" onChange={this.handleTitleChange} placeholder="Titre" value={this.state.title}/>
				</div>
				<div>
					<input name="season" id="season" type="number" step="1" min="1" onChange={this.handleSeasonChange} onKeyUp={this.handleSeasonChange} onMouseUp={this.handleSeasonChange} placeholder="Saison" value={this.state.season}/>
				</div>
				<div>
					<input name="episode" id="episode" type="number" step="1" min="1" onChange={this.handleEpisodeChange} onKeyUp={this.handleEpisodeChange} onMouseUp={this.handleEpisodeChange} placeholder="Épisode" value={this.state.episode}/>
				</div>
				<div>
					<button onClick={this.handleSubmit} disabled={!this.state.submittable}>Marquer comme vu</button>
				</div>
			</form>
		);
	}
});

var EpisodeManager = React.createClass({

	addEpisode: function (title, season, episode) {
			
		var data = JSON.stringify({title: title, season: season, episode: episode});
	
		fetch ('http://127.0.0.1:9312', { 
			method: 'POST',
			headers: {'Accept': 'application/json'},
			body: data,
			mode: 'cors'
		})
		.then(function (response){
			if (!response.ok){
				alert('L\'épisode n\'a pas pu être sauvegardé sur le serveur distant (code ' + e.status + ')');
			}
		}).catch(function (err){
			alert('Une erreur réseau s\'est produite : ' + err);
		});
	},

	render: function (){
		return <div className={"grid"}><ListEpisode pollInterval={1000}/><EpisodeForm onEpisodeSubmit={this.addEpisode}/></div>;
	}

});

render(<EpisodeManager/>, document.getElementById('content'));
