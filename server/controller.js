var DAL = require('./dal');
var URL = require('url');

var Controller = function () {

	this.dal = new DAL();
	
	this.getAll = function(request, response){
		var status = this.dal.countEpisodes() > 0 ? 200 : 204;

		var body = JSON.stringify(this.dal.getAll());
		response.writeHead(status, {
			"Content-Type": "application/json",
			"Content-Length": body.length,
			"Access-Control-Allow-Origin": "*",
			"Access-Control-Allow-Headers": "X-Requested-With"
		});
		response.write(body);
		response.end();
	}
	
	this.getEpisode = function(request, response){
		var params = URL.parse(request.url, true).query;

		if (params.hasOwnProperty('id')){
			if (this.dal.episodeExists(params.id)){
				var body = JSON.stringify(this.dal.getEpisode(params.id));
				response.writeHead(200, {
					"Content-Type": "application/json",
					"Content-Length": body.length,
					"Access-Control-Allow-Origin": "*",
					"Access-Control-Allow-Headers": "X-Requested-With"
				});
				response.write(body);
			} else {
				response.writeHead(404, {
					"Content-Type": "text/html",
					"Access-Control-Allow-Origin": "*",
					"Access-Control-Allow-Headers": "X-Requested-With"
				});
				response.write('<html><body><h1>404 - Not found</h1></body></html>');
			}
		} else {
			response.writeHead(400, {"Content-Type": "text/html"});
			response.write('<html><body><h1>400 - Bad request</h1></body></html>');
		}
		
		response.end();
	}
	
	this.addEpisode = function(request, response){

		var requestBody = '';

		request.on('data', function (data){
			requestBody += data;
		});

		request.on('end', function (){
			var episode = null;

			try {
				episode = JSON.parse(requestBody);
				console.log(requestBody);
			} catch (e) {
				console.error('Invalide json sended by client : ', e, '\nin :"', requestBody, '"');
				response.writeHead(422, {
						"Content-Type": "application/json",
						"Access-Control-Allow-Origin": "*",
						"Access-Control-Allow-Headers": "X-Requested-With"
					});
				response.write('{ "error": {"type": "Unprocessable entity", "cause": "'+e+'"}}');
			}

			if (episode != null) {
				if (episode.hasOwnProperty('title') && episode.hasOwnProperty('season') && episode.hasOwnProperty('episode')) {
					episode = this.dal.addEpisode(episode);
					response.writeHead(201, {
						"Content-Type": "application/json",
						"Access-Control-Allow-Origin": "*",
						"Access-Control-Allow-Headers": "X-Requested-With"
					});
					response.write(JSON.stringify(episode));
				} else {
					response.writeHead(422, {
						"Content-Type": "application/json",
						"Access-Control-Allow-Origin": "*",
						"Access-Control-Allow-Headers": "X-Requested-With"
					});
					response.write('{ "error": {"type": "Unprocessable entity", "cause": "Submitted entity has to have \"title\", \"season\" and \"episode\" properties."}}');
				}
			}

			response.end();
		}.bind(this));
	}
}

module.exports = Controller
