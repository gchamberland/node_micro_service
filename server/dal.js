
var DAL = function () {

	this.episodes = new Map();

	
	this.addEpisode = function(episode){
		episode.id = Math.floor(Math.random() * 1000000).toString();

		this.episodes.set(episode.id, episode);
		
		return episode;
	}
	
	this.countEpisodes = function(){
		return this.episodes.size;
	}
	
	this.getAll = function(){

		var result = [];

		this.episodes.forEach(function(value, key){
			result.push(value);
		});

		return result;
	}
	
	this.episodeExists = function(id){
		return this.episodes.has(id);
	}
	
	this.getEpisode = function(id){
		return this.episodes.get(id);
	}
}

module.exports = DAL;
