
var url = require('url');

var Router = function () {

	this.posts = new Map();
	this.gets = new Map();

	this.addGet = function(path, callback, context){
		this.gets.set(path, { callback : callback, context : context });
	}
	
	this.addPost = function(path, callback, context){
		this.posts.set(path, { callback : callback, context : context });
	}

	this.process = function (routes, request, response) {

		var pathname = url.parse(request.url).pathname;

		for (var route of routes.keys()){
            var expr = new RegExp('^'+route+'$')
			if (expr.test(pathname)){
				var context = routes.get(pathname).context;
				routes.get(route).callback.call(context, request, response);
				return true;
			}
		}
		return false;
	}
	
	this.handle = function(request, response){

		var method = request.method;

		switch (method){
			
			case 'GET':
				if (!this.process(this.gets, request, response)){
					response.writeHead(404, {"Content-Type": "text/html"});
					response.write('<html><body><h1>404 - Not found</h1></body></html>');
					response.end();
				}
			break;
			
			case 'POST':
				if (!this.process(this.posts, request, response)){
					response.writeHead(404, {"Content-Type": "text/html"});
					response.write('<html><body><h1>404 - Not found</h1></body></html>');
					response.end();
				}
			break;
			
			default:
				response.writeHead(405, {"Content-Type": "text/html"});
				response.write('<html><body><h1>405 - Method not allowed</h1></body></html>');
				response.end();
			break;
		}
	}
}

module.exports = Router;
