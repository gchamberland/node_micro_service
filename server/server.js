
var http = require('http');
var Router = require('./router');
var Controller = require('./controller');

var Server = function (port) {
	
	this.start = function (){
		var router = new Router();
		var controller = new Controller();

		router.addGet('/', controller.getAll, controller);
		router.addGet('/episode', controller.getEpisode, controller);
		router.addPost('/', controller.addEpisode, controller);

		http.Server(function (request, response){
			router.handle(request, response);
		}).listen(port);
	}
}

module.exports = Server;

